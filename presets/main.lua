--Frame demo with style switching, animations and subframes (and a useful dragging eventListener function)
gui = require 'gui'
love.window.setMode(1280,720,{vsync=true})

local mgui = {}
mgui.mainButton = {
	backgroundImage = love.graphics.newImage('btn_bg.png'),
    margins = {10,10,10,10},
    backgroundSize = 'fit',
    backgroundColor = {0,0,0,0},
    backgroundImageColor = {200, 200, 200, 255},
    foregroundColor= {25, 25, 25, 255},
    font = love.graphics.newFont('/data/fonts/saxmono.ttf', 25),
    text = {
        align = 'center',
    }
}
mgui.mainButtonOver = {
	backgroundImage = love.graphics.newImage('btn_bg.png'),
    margins = {10,10,10,10},
    backgroundSize = 'fit',
    backgroundColor = {0,0,0,0},
    backgroundImageColor = {30, 230, 30, 255},
    foregroundColor= {25, 25, 25, 255},
    font = love.graphics.newFont('/data/fonts/saxmono.ttf', 25),
    text = {
        align = 'center',
    }
}
mgui.mainButtonPressed = {
	backgroundImage = love.graphics.newImage('btn_bg.png'),
    margins = {10,10,10,10},
    backgroundSize = 'fit',
    backgroundColor = {0,0,0,0},
    backgroundImageColor = {100, 100, 230, 255},
    foregroundColor= {25, 25, 25, 255},
    font = love.graphics.newFont('/data/fonts/saxmono.ttf', 25),
    text = {
        align = 'center',
    }
}
mgui.frame = {
	backgroundColor = {0,0,0,0}
}
mgui.title = {
	backgroundColor = {0,0,0,0},
	foregroundColor = {230, 230, 230, 255},
	font = love.graphics.newFont('/data/fonts/Nunito.ttf', 55),
}
mgui.subframe = {
	backgroundColor = {2,2,2,255},
	outlineColor = {230,230,230,255},
	outline = 1
}
mgui.subframeButton = {
	backgroundColor = {2,2,2,255},
	outlineColor = {230,230,230,255},
	foregroundColor = {230, 230, 230, 255},
	padding = {3, 0, 3, 3},
	margins = {5, 3, 5, 3},
	outline = 1,
}

mgui.animOnDrawFrame = function (style, prog, event, element)
	frame.type.layout.sfrm.h = 500*prog
	--element.w = 1280/2+1280/2*prog
end
mgui.titleOnDraw = function (style, prog, event, element)
	style.foregroundColor[4] = prog*255
end
mgui.animOnDrawButton = function (style, prog, event, element)
	element.cValues.h = prog*element.h
	style.backgroundImageColor[4]=255*prog
	style.foregroundColor[4]=255*prog
end
mgui.animOnEnter = function (style, prog, event, element)
	style.backgroundImageColor[1] = 200-prog*170
	style.backgroundImageColor[2] = 200+prog*30
	style.backgroundImageColor[3] = 200-prog*170
end
mgui.animOnExit = function (style, prog, event, element)
	style.backgroundImageColor[1] = 30+prog*170
	style.backgroundImageColor[2] = 230-prog*30
	style.backgroundImageColor[3] = 30+prog*170
end
mgui.animOnPress = function (style, prog, event, element)
	style.backgroundImageColor[1] = 30+70*prog
	style.backgroundImageColor[2] = 230-prog*130
	style.backgroundImageColor[3] = 30+prog*200
end
mgui.animOnRelease = function (style, prog, event, element)
	style.backgroundImageColor[1] = 100-70*prog
	style.backgroundImageColor[2] = 100+prog*130
	style.backgroundImageColor[3] = 230-prog*200
end
local frameClass = gui.template.new('frame', mgui.frame)
local subframeClass = gui.template.new('frame', mgui.subframe)
local subframeBtnClass = gui.template.new('button', mgui.subframeButton)
local subframeTxtClass = gui.template.new('text', mgui.subframeButton)
local buttonClass = gui.template.new('button', mgui.mainButton)
local titleClass = gui.template.new('text', mgui.title)
local sliderClass = gui.template.new('slider',{})
local checkboxClass = gui.template.new('checkbox',{})

buttonClass:addAnimation('canvasinit', mgui.animOnDrawButton, 1)
buttonClass:addAnimation('entered', mgui.animOnEnter, 0.2)
buttonClass:addAnimation('exited', mgui.animOnExit, 0.2)
buttonClass:addAnimation('pressed',mgui.animOnPress, 0.1)
buttonClass:addAnimation('released',mgui.animOnRelease, 0.1)
buttonClass:addStyleSwitch('entered', 'exited',  mgui.mainButtonOver)
buttonClass:addStyleSwitch('pressed', 'released',mgui.mainButtonPressed)

local play = buttonClass:newElement('Play')
local slider = sliderClass:newElement()
local load = buttonClass:newElement('Load')
local settings = buttonClass:newElement('Settings')
local exit = buttonClass:newElement('Exit')
local checkbox = checkboxClass:newElement(true)
local title = titleClass:newElement('Introducing\nframes.')
title:addAnimation('canvasinit', mgui.titleOnDraw, 1)

sfrmtxt1 = subframeTxtClass:newElement('subframe')
sfrmtxt2 = subframeTxtClass:newElement('subframe')
sfrmtxt3 = subframeTxtClass:newElement('subframe')
sfrmtxt4 = subframeTxtClass:newElement('subframe')
sfrmtxt5 = subframeTxtClass:newElement('subframe')
sfrmbtn1 = subframeBtnClass:newElement('regular layout')
sfrmbtn2 = subframeBtnClass:newElement('')
sfrmbtn3 = subframeBtnClass:newElement('relative position')
sfrmbtn4 = subframeBtnClass:newElement('drag me')
sfrmbtn4:addEventListener('pressed',
	function (event, element)
    	element:addEventListener('update', 
    	function (event, element, id)
        	frame.type.layout.sfrm.w = gui.inputBuffer.mouse.current.x+30-subframe.x
			frame.type.layout.sfrm.h = gui.inputBuffer.mouse.current.y+10-subframe.y
			frame.type.mkLayout = true
        	if not gui.inputBuffer.mouse.pressEvent then
            	element:removeEventListener('update',id)
        	end
    	end)
	end)
sfrmbtn5 = subframeBtnClass:newElement('')
sfrmbtn6 = subframeBtnClass:newElement('')


subframe = subframeClass:newElement({
	elements = {
		{element = sfrmtxt1, index = 'text'},
		{element = sfrmtxt2, index = 'text1'},
		{element = sfrmtxt3, index = 'text2'},
		{element = sfrmtxt4, index = 'text3'},
		{element = sfrmtxt5, index = 'text4'},
		{element = sfrmbtn1, index = 'button1'},
		{element = sfrmbtn2, index = 'button2'},
		{element = sfrmbtn3, index = 'button3'},
		{element = sfrmbtn4, index = 'button4'},
		{element = sfrmbtn5, index = 'button5'},
		{element = sfrmbtn6, index = 'button6'},
	},
	layout = {
		button2 = {
			size = 'relative',
			w = 40,
			h = 15,			
		},
		button3 = {
			size = 'relative',
			w = 50,
			h = 15,	
			position = 'absolute',
			right = 0,		
			bottom = 30,
		},
		button4 = {
			position = 'absolute',
			right = 0,
			bottom = 0,			
		},
		button5 = {
			size = 'relative',
			w = 100,
			h = 15,			
		},
		button6 = {
			size = 'relative',
			w = 100,
			h = 15,			
		},
	}
})

frame = frameClass:newElement({
	elements = {
		{element = play, index = 'play'},
		{element = load, index = 'load'},
		{element = settings, index = 'settings'},
		{element = exit, index = 'exit'},
		{element = title, index = 'title'},
		{element = slider, index = 'slider'},
		{element = subframe, index = 'sfrm'},
		{element = checkbox, index = 'checkbox'}
	},
	layout = {
		play = {
			position = 'relative',
			top = 15,
			left = 35,
			size = 'relative',
			w = 30,
			h = 20,
		},
		load = {
			position = 'relative',
			top = 35,
			left = 35,
			size = 'relative',
			w = 30,
			h = 20,
		},
		settings = {
			position = 'relative',
			top = 55,
			left = 35,
			size = 'relative',
			w = 30,
			h = 20,
		},
		exit = {
			position = 'relative',
			top = 75,
			left = 35,
			size = 'relative',
			w = 30,
			h = 20,
		},
		title = {
			position = 'absolute',
			top = 1,
			right = 1,
			size = 'relative',
			w = 30,
			h = 20,
		},
		sfrm = {
			position = 'relative',
			top = 30,
			left = 1,
			size = 'absolute',
			w = 300,
			h = 500,
		},
		slider = {
			size = 'absolute',
			w = 200,
			h = 50,
			position = 'relative',
			top = 30,
			left = 80,
		},
		checkbox = {
			size = 'absolute',
			w = 50,
			h = 50,
			position = 'relative',
			top = 40,
			left = 80,
		}
	}
})

subframe:addAnimation('canvasinit',mgui.animOnDrawFrame,1)

function love.load()
	dtt = 0
	el = {}
end

function love.update(dt)

	dtt = dtt+dt

    --[[if play.prevEvents then
        for i, e in pairs(el) do
            e.time = e.time+dt
        end
        for i, e in pairs(play.prevEvents) do
            if el[i] then
                el[i].time = 0
            else
                el[i] = {}
                el[i].time = 0
            end
        end
    end]]
end

function love.draw()
	love.graphics.reset()
	love.graphics.setColor(255,255,255)
	love.graphics.setFont(mgui.mainButton.font)
	love.graphics.print(love.timer.getFPS())
	love.graphics.print(tostring(checkbox.type.state),0, 20)
	local stats = love.graphics.getStats()
	local str = string.format("Estimated amount of texture memory used: %.2f MB", stats.texturememory / 1024 / 1024)
    love.graphics.print(str, 0, 40)
	local n = 0
	--[[if avgtimers then
	for i, e in pairs(avgtimers) do
		love.graphics.print(i..':'..e*1000,0, 50+25*n)
		n = n +1
	end
	end
	love.graphics.print(#play.eventListeners.pressed,0,15)]]
	--[[if play.stylePriority and play.styleBuffer then
		love.graphics.print(play.stylePriority..#play.styleBuffer)
	end
	c = 0
	for i, e in pairs(el) do
        c = c + 1
        if e.time<2.5 then
            love.graphics.setColor(25,255,25,255-255*(e.time/2.5))
            love.graphics.print(i, 1, 15+15*c)
        end
    end
	--]]
end

function love.keypressed(key)
	frame:drawPersistent(0, 0, 1280, 720)
end