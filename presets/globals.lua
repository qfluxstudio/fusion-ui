local gui = require 'gui'
love.window.setMode(1280,720,{vsync=false})

local tS = {
	backgroundColor = {10, 10, 10, 10},
	margins = {10, 0, 0, 0},
	padding = {1, 0, 1, 0}
}
local testNum = 0
local testClass = gui.template.new('text',tS)
--testClass:addStyleSwitch('entered','exited',tS)

local frmElems = {
	elements = {{element = testClass:newElement("Global variables:")}}
}

for i, e in pairs(_G) do
	testNum = testNum+1
	table.insert( frmElems.elements, {element = testClass:newElement(i..' = '..tostring(e))})
end

local frm = gui.element.newElement('frame', frmElems, {})

frm:addEventListener('dragged',function (event, element) 
	frm.x = frm.x + event.dx
	frm.y = frm.y + event.dy 
end)

frm:drawPersistent(160,10,300,620)

function love.draw()
	love.graphics.setColor(255,255,255,255)
	love.graphics.print(love.timer.getFPS())
	love.graphics.print('e:'..testNum,0,15)
	local stats = love.graphics.getStats()
	local str = string.format("Vram usage: %.2f MB", stats.texturememory / 1024 / 1024)
	love.graphics.print(str, 0, 40)

	n=0
	if avgtimers then
		for i, e in pairs(avgtimers) do
			love.graphics.print(i..':'..e*1000,0, 60+25*n)
			n = n +1
		end
	end
end