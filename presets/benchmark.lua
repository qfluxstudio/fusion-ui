gui = require 'gui'
love.window.setMode(1280,720,{vsync=false})

tS = {
	backgroundColor = {200, 200, 200, 255}
}
testNum = 10000
testClass = gui.template.new('text',{})

frmElems = {
	elements = {}
}

for i = 1, testNum do
	elements[i] = {element = testClass:newElement(tostring(i))}
end

frm = gui.element.newElement('frame', frmElems, {})

frm:drawPersistent(120,20,1100,680)

function love.draw()
	love.graphics.print(love.timer.getFPS())
	love.graphics.print('elements:'..testNum,0,15)
	local stats = love.graphics.getStats()
	local str = string.format("Vram usage: %.2f MB", stats.texturememory / 1024 / 1024)
	love.graphics.print(str, 0, 40)
	n=0
	if avgtimers then
		for i, e in pairs(avgtimers) do
			love.graphics.print(i..':'..e*1000,0, 60+25*n)
			n = n +1
		end
	end
end