--This has some useful code for testing and some basic style configurations
gui = require 'gui'
--styles = require 'styles'
love.window.setMode(800,600,{vsync=true, msaa=8})
mainButton = {
    backgroundImage = love.graphics.newImage('btn_bg.png'),
    margins = {10,10,10,10},
    backgroundSize = 'fit',
    backgroundColor = {0,0,0,0},
    backgroundImageColor = {20, 200, 20, 255},
    foregroundColor= {255, 255, 255, 255},
    font = love.graphics.newFont('/data/fonts/saxmono.ttf', 25),
    text = {
        align = 'center',
    }
}
sbtn = {
    margins = {10,10,10,10},
    backgroundColor = {100,100,100,255},
    foregroundColor= {255, 255, 255, 255},
    font = love.graphics.newFont('/data/fonts/saxmono.ttf', 25),
    text = {
        align = 'center',
    }
}

buttonPress = {
    backgroundImage = love.graphics.newImage('btn_bg.png'),
    backgroundSize = 'fit',
    backgroundColor = {0,0,0,0},
    backgroundImageColor = {20, 200, 200, 255},
    foregroundColor= {255, 255, 255, 255},
    font = love.graphics.newFont('/data/fonts/saxmono.ttf', 25),
    text = {
        align = 'center',
    }
}

tAnim = function(style, progress, event)
    style.backgroundImageColor[3] = 20+180*progress
end

bp = gui.style.newStyle(buttonPress)

ind = 0

function love.load()
    tc = gui.template.newClass('textBox',nil, mainButton)
    txtc = gui.template.newClass('text')
    tbc = gui.template.newClass('button',nil, sbtn)
    rightBtn = tbc:newElement('>')
    rightBtn:addEventListener('pressed',function (event, element)
        element:addEventListener('update', 
        function (event, element, id)
            frm.w = gui.inputBuffer.mouse.current.x+30-frm.x
            if not gui.inputBuffer.mouse.current.down then
                element:removeEventListener('update',id)
            end
        end)
    end)
    play = tc:newElement('Aligned to bottom')
    play:addEventListener('changed', function (event, element) play2.content = element.content end)
    play2 = txtc:newElement('Random text')
    play3 = txtc:newElement('Multi \n line')
    play4 = txtc:newElement('####')
    play5 = txtc:newElement('Empty')
    frmc = gui.element.newClass('frame',nil)
    frm = frmc:newElement({
        elements = {
            {element = play, index = 'button1'},
            {element = play2, index = 'button2'},
            {element = play3, index = 'button3'},
            {element = play4, index = 'button4'},
            {element = play5, index = 'button5'},
            {element = rightBtn, index = 'rbtn'}
        },
        layout = {
            button1 = {
                size = 'relative',
                w = 85,
                h = 15,
                position = 'absolute',
                bottom = 0
            },
            rbtn = {
                size = 'relative',
                w = 15,
                h = 100,
                position = 'absolute',
                right = 0
            }
        }
    })
    
    print('frm draw')
    frm:drawPersistent(300, 100, 300, 400)
    --play:drawPersistent(100, 10, 300, 100)
 root, el = {}, {}
end

function love.update(dt)
   root = {}
    
    for i, e in pairs(rightBtn) do
        table.insert(root, i)
    end
    if rightBtn.prevEvents then
        for i, e in pairs(el) do
            e.time = e.time+dt
        end
        for i, e in pairs(rightBtn.prevEvents) do
            if el[i] then
                el[i].time = 0
            else
                el[i] = {}
                el[i].time = 0
            end
        end
    end
end

function love.draw()
    love.graphics.reset()
    love.graphics.print(love.timer.getFPS())

    local c = 0
    for i, e in pairs(el) do
        c = c + 1
        if e.time<2.5 then
            love.graphics.setColor(25,255,25,255-255*(e.time/2.5))
            love.graphics.print(i, 1, 15+15*c)
        end
    end
]]
    local w, h = play2:getSize()
    love.graphics.print(w..' '..h, 0, 15)
    love.graphics.reset()
    


    --love.graphics.print('c1: '..play.cStyle.backgroundImageColor[3])
--[[    love.graphics.print('current capture: '..tostring(gui.inputBuffer.keyboard.currentCapture))
    love.graphics.print('play content id: '..tostring(play.type.capID),0, 15)
    love.graphics.print('inp:'..tostring(gui.inputBuffer.keyboard.intxt),0, 30)
    love.graphics.print(' '..#gui.element.elementBuffer)
    ind = 0]]
end

function gui.keypressed(key)
end

--[[
function love.draw()
    for i = 1, 10 do
    love.graphics.rectangle('fill',100,100,200,200)
    love.graphics.print('asdasdasdasdasd',100,100)
    love.graphics.line(10,10,100,100)
    
    x = 100/200+200/2
    end
end]]

--[[
    play = tc:newElement('')
    play2 = tc:newElement('')
    play:addStyleSwitch('captured', 'uncaptured', bp)
    play:addStyleSwitch('entered', 'exited', bp)
    play:addAnimation('entered', tAnim, 1)
    play2:addStyleSwitch('captured', 'uncaptured', bp)
    play2:addStyleSwitch('entered', 'exited', bp)

    play:drawPersistent(100, 100,  250, 30)
    play2:drawPersistent(100, 140,  250, 30)]]