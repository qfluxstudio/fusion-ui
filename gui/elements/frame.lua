--[[--------------------------------------------------
	Fusion UI by qfx (qfluxstudios@gmail.com) 
	Copyright (c) 2017-2018 Elmārs Āboliņš
	gitlab.com/project link here 
----------------------------------------------------]]

local path = string.sub(..., 1, string.len(...) - string.len(".elements.frame"))
local gui = require(path .. ".dummy")

---The frame element, this element is used to 'contain' other elements
--As such, it's content is the most complex
--[[
	Content format:
	content = {
		--Inside of this field you add all the elements that will be inside of the frame
		elements = {
			{element = testButtonClass:newElement('Test!'), index = 'button'},
			{element = textClass:newElement('Testing text'), index = 'text'}
		},
		--Inside of the layout field you specify the layout for the elements inside of the frame
		layout = {
			button = {
				position = 'absolute',
				x = 0,
				y = 0,
				w = 100,
				h = 10
			},
			text = {
				
			}
		}
	}

	all layout properties:
	position = absolute (px), relative(%), nil(auto) 
	size = absolute(px), relative(%), calculated(auto)
	w = 100
	h = 100
	left = 100
	right = 100
	top = 100
	bottom = 100
]]
--@module slider
local frame = {}
frame.__index = frame

local misc = gui.elementLib.misc
local state = gui.elementLib.state
local string = gui.elementLib.string

--[[
	Content format:
	content = {
		elements = {
			{element = testButtonClass:newElement('Test!'), index = 'button'},
			{element = textClass:newElement('Testing text'), index = 'text'}
		},
		layout = {
			button = {
				position = 'absolute',
				x = 0,
				y = 0,
				w = 100,
				h = 10
			},
			text = {
				
			}
		}
	}

	all layout properties:
	position = absolute, relative
	size = absolute, relative, calculated
	w = 100
	h = 100
	left = 100
	right = 100
	top = 100
	bottom = 100
]]

function frame.new(content)
	local fr = setmetatable({
		elementContainers = content,
		layout = content.layout or {},
		layoutSettings = content.layoutSettings or {},
		all_nl = content.all_nl or false,
		init = true,
		vOffset = 0,
		type = 'frame',
		mkLayout = true,
		elements = {}
	}, frame)

	for i, eCont in ipairs(fr.elementContainers) do
		eCont.element:addEventListener('changed', fr.elementUpdate, fr, i)
		eCont.element:addEventListener('stylechange', fr.elementUpdate, fr, i)
		eCont.element:addEventListener('styleswitch', fr.elementUpdate, fr, i)
		eCont.element:addEventListener('redrawn', fr.elementRender, fr, i)
		eCont.element:addEventListener('canvasinit', fr.elementRender, fr, i)
		--eCont.element:addEventListener('stylechange', fr.elementUpdate, fr)
		if eCont.index then
			fr.elements[eCont.index] = eCont.element
		end
	end

	return fr
end

function frame:cleanUp()
	gui.input.removeBox(self.box)
	self.box = nil

	for i, eCont in ipairs(self.elementContainers) do
		eCont.element:unDraw()
	end
end

function frame:elementUpdate()
	self.mkLayout = true
	self.recalcvp = true
end

function frame:elementRender()
	self.redraw = true
	self.recalcvp = true
end

function frame:isWithinViewport(eCont)
	local vo = self.vOffset or 0
	--print(vo)
	local viewport = {
		x1 = 0,
		x2 = self.w,
		y1 = (-vo),
		y2 = (-vo) + self.h
	}
	if viewport.y1 < eCont.props.y + eCont.props.h then
		if viewport.y2 > eCont.props.y then
			return true
		else
			return false, 'below'
		end
	else
		return false, 'above'
	end
end

function frame:recalcViewport()
	self.redraw = true
	self.inPortElements = {}
	--local grace = 100
	--[[
	if self.w and self.h then
		for i, e in ipairs(self.elementContainers) do
			if self:isWithinViewport(e) then
				table.insert(self.inPortElements, e)
				--grace = 100
			elseif self.recalcAboveViewport then
				self.grace = self.grace-1
				--grace = grace - 1
			end
			--if grace<=0 then
				--break
			--end
		end
	else
		print('wtf')
	end]]
	--Let's try insert sort

	if self.w and self.h then
		local lowernum = 1
		local uppernum = #self.elementContainers
		local result, try, desc

		--finding where in 1/2 it is
		local iterations = 0
		repeat
			try = math.floor((uppernum - lowernum) / 2) + lowernum

			result, desc = self:isWithinViewport(self.elementContainers[try])

			if not result then
				if desc == 'above' then
					lowernum = try
				else
					uppernum = try
				end
			end
			iterations = iterations + 1
		until result

		local reachedTop = false
		local reachedBottom = false
		local topIter = 1
		local bottomIter = 1

		repeat
			if not reachedTop then
				if try - topIter <= 0 then
					reachedTop = true
				else
					reachedTop = not self:isWithinViewport(self.elementContainers[try - topIter])
					topIter = topIter + 1
				end
			end

			if not reachedBottom then
				if try + bottomIter > #self.elementContainers then
					reachedBottom = true
				else
					reachedBottom = not self:isWithinViewport(self.elementContainers[try + bottomIter])
					bottomIter = bottomIter + 1
				end
			end
		until reachedBottom and reachedTop

		for i = try - topIter, try + bottomIter do
			table.insert(self.inPortElements, self.elementContainers[i])
		end
	end
end

function frame:sliderChange()
	self.recalcvp = true
end

function frame:update(x, y, w, h, content, style, elem)

	if not self.elem then
		self.elem = elem
		self.elem:addEventListener('redrawFrm', self.elementRender, self)
	end

	if self.redraw then
		elem:emitEvent('changed')
	end

	self.w = w
	self.h = h

	if not self.box then
		self.box = gui.input.addBox(x, y, self.w, self.h, style.z, 1, true, function(event, eventType)
			if eventType == 'touchmoved' then
				if math.abs(math.abs(event.startY) - math.abs(event.y)) > 50 then

					self:elementRender()

					if self.slider then

						if (self.slider.type.current - event.dy) < 0 or (self.slider.type.current - event.dy) > self.slider.type.max then
							print(self.slider.type.current .. '+' .. event.dy)
						else
							self.slider.type.current = self.slider.type.current - event.dy
							self.slider:emitEvent('changed', { value = self.slider.type.current - event.dy })
						end

						return false
					end
				end
			elseif eventType == 'touchpressed' then
				if self.slider then
					self:elementRender()
					return false
				end
			elseif eventType == 'touchreleased' then
				if math.abs(math.abs(event.startX) - math.abs(event.x)) > 50 then

					self:elementRender()

					if self.slider then
						if (self.slider.type.current - event.dy) < 0 or (self.slider.type.current - event.dy) > self.slider.type.max then
						else
							self.slider.type.current = self.slider.type.current - event.dy
							self.slider:emitEvent('changed', { value = self.slider.type.current - event.dy })
						end

						self.vOffsetVelocity = -event.dy

						return false
					end
				end
			end
			return true
		end)
	end

	if self.mkLayout then
		self:makeLayout(style, self.w, self.h)
		self.recalcvp = true
		self.elem:emitEvent('changed')
	end

	if self.recalcvp then
		self:recalcViewport()
		self.recalcvp = false
	end

	--Calculate elements to update
	local elements

	if not self.inPortElements or self.init then
		elements = self.elementContainers
	else
		elements = self.inPortElements
	end

	for i, eCont in ipairs(elements) do
		local p = eCont.props
		if not (p.y + self.vOffset - p.h > self.h or p.y - self.vOffset + p.h < 0) or self.init then
			if self.redraw then
				--todo: typename to has children, rerender to redraw
				if eCont.element.typeName == 'frame' then
					eCont.element.type.rerender = true
				end
			end
			eCont.element:draw(p.x + x, p.y + y + self.vOffset, p.w, p.h, nil, false)
			eCont.element:update(0, false)
		elseif eCont.element.style.position == 'fixed' then
			eCont.element:draw(p.x + x, p.y + y, p.w, p.h, nil, false)
			eCont.element:update(0, false)
		else
			eCont.element.type:cleanUp()
		end
		self.init = false
	end
	if self.slider then
		self.slider:draw(self.w - 25 + x, 0 + y + 15, 25, self.h, nil, false)
		self.slider:update(0, false)

		if self.vOffsetVelocity then

			if self.slider.type.max > self.slider.type.current + self.vOffsetVelocity then
				self.slider.type.current = self.slider.type.current + self.vOffsetVelocity
				self.slider:emitEvent('changed', { value = self.slider.type.current + self.vOffsetVelocity })

				self.vOffsetVelocity = self.vOffsetVelocity * (1 - (1 / (self.lastUpdate - self.currentTime)))

				if math.abs(self.vOffsetVelocity) <= 9 then
					self.vOffsetVelocity = nil
				end
			else
				self.slider.type.current = self.slider.type.max
				self.vOffsetVelocity = nil
				self.slider:emitEvent('changed', { value = self.slider.type.max })
			end
		end

		self.vOffset = -self.slider.type.current
	end

	gui.input.closeParent()

	self.box.w = self.w
	self.box.h = self.h
	self.box.x = x
	self.box.y = y

	local st = state.check(self.box, {
		'pressed',
		'released',
		'entered',
		'exited',
		'pressEvent',
		'over',
		'down',
		'dropped',
		'dragged'
	})
	return {
		state = st,

		drawX = x,
		drawY = y,

		static = false,

		w = self.w,
		h = self.h
	}
end

function frame:makeLayout(style, width, height, maxH, maxW)
	local noHeight = false
	if not height then
		noHeight = true
	end
	local h = height or maxH

	local noWidth = false
	if not width then
		noWidth = true
	end
	local w = width or maxW

	local elemTree = {}
	for index, eCont in ipairs(self.elementContainers) do
		local elementProps = self.layout[eCont.index] or {}

		eCont.props = gui.childHelper.get_sizes(elementProps, eCont.element, w - gui.childHelper.calcNum(style.padding[3], w, 0) - gui.childHelper.calcNum(style.padding[1], w, 0), h)
		if not eCont.props.w then
			assert(false)
		end

		if eCont.props.y == 'calculated' or eCont.props.x == 'calculated' then
			table.insert(elemTree, eCont)
			eCont.props.y = 0
		end
	end

	local curH = gui.childHelper.calcNum(style.padding[2], h, 0)
	local curLH = 0
	local curW = gui.childHelper.calcNum(style.padding[1], w, 0)
	local maxWidth = 0
	local lastHMargin = 0
	local lastWMargin = 0
	for i, eCont in ipairs(elemTree) do
		local margin = {}
		margin[1] = gui.childHelper.calcNum(eCont.element.style.margins[1], w, 0)
		margin[2] = gui.childHelper.calcNum(eCont.element.style.margins[2], h, 0)
		margin[3] = gui.childHelper.calcNum(eCont.element.style.margins[3], w, 0)
		margin[4] = gui.childHelper.calcNum(eCont.element.style.margins[4], h, 0)

		lastWMargin = margin[3]
		lastHMargin = margin[4]

		if self.all_nl then
			eCont.props.y = curH + margin[2]
			eCont.props.x = curW + margin[1]
			curLH = eCont.props.h + margin[4] + margin[2]
			curH = curH + curLH
		else
			if curW + eCont.props.w + margin[1] + margin[3] < w then
				eCont.props.y = curH + margin[2]
				eCont.props.x = curW + margin[1]

				curW = curW + eCont.props.w + margin[1] + margin[3]

				if maxWidth < curW then
					maxWidth = curW
				end

				if curLH < eCont.props.h + margin[4] + margin[2] then
					curLH = eCont.props.h + margin[4] + margin[2]
				end
			else
				eCont.props.y = curH + curLH + margin[2]
				eCont.props.x = gui.childHelper.calcNum(style.padding[1], w, 0)

				if maxWidth < curW then
					maxWidth = curW
				end

				if curW < (w - 25) / 2 then
					curH = curH + curLH
					curLH = eCont.props.h + margin[4] + margin[2]
				else
					curH = curH + curLH + margin[2] + eCont.props.h + margin[4]
					curLH = 0
				end

				curW = gui.childHelper.calcNum(style.padding[1], w, 0)--eCont.props.w + margin[1] + margin[3] + gui.childHelper.calcNum(style.padding[1], w, 0)

			end
		end
	end

	if curLH > curH then
		curH = curLH + gui.childHelper.calcNum(style.padding[4], h, 0) + lastHMargin
	end

	if noWidth then
		self.w = maxWidth + gui.childHelper.calcNum(style.padding[3], w, 0)
	else
		self.w = w
	end

	if self.h and curH > self.h then
		if self.slider then
			self.slider.type.max = curH - self.h + 30
		else
			self.slider = gui.element.newElement('slider', { min = 0, max = curH - self.h, step = 1, current = 0 }, {})

			--[[	self.slider:addEventListener('changed', self.elementUpdate, self)
				self.slider:addEventListener('stylechange', self.elementUpdate, self)
				self.slider:addEventListener('styleswitch', self.elementUpdate, self)]]
			self.slider:addEventListener('redrawn', self.sliderChange, self)
			self.slider.doNotSleep = true
		end
		self.h = height
	else
		self.slider = nil
		self.vOffset = 0
		if noHeight == true then
			self.h = curH
		else
			self.h = h
		end
	end

	--Sanity check
	if self.w < 5 then
		self.w = 5
	end
	if self.h < 5 then
		self.h = 5
	end
end

function frame:getSize(_, style, fW, fH, elem)
	local w, h
	w = self.w or nil
	h = self.h or nil

	self.elem = elem

	self:makeLayout(style, w, h, fH, fW)

	self.elem:emitEvent('changed')

	return self.w, self.h
end

function frame:render(x, y, w, h, content, style)
	style:drawBackground(x, y, w, h)
	gui.platform.setColor(style.foregroundColor)

	--If the layout changed, then re-render all elements
	if self.mkLayout then
		for i, eCont in ipairs(self.elementContainers) do
			local p = eCont.props
			if not (p.y + self.vOffset - p.h > self.h or p.y - self.vOffset + p.h < 0) then
				local a, b = gui.platform.getBlendMode()
				gui.platform.setBlendMode("alpha", "premultiplied")

				eCont.element:render(eCont.props.x, eCont.props.y + self.vOffset)

				gui.platform.setBlendMode(a, b)
			end
		end
		if self.slider then
			self.slider:render(self.w - 25, 0 + 15)
		end
		self.mkLayout = false
	end

	if self.redraw or self.rerender then
		for i, eCont in ipairs(self.elementContainers) do
			local p = eCont.props
			if not (p.y + self.vOffset - p.h > self.h or p.y - self.vOffset + p.h < 0) then
				local a, b = gui.platform.getBlendMode()
				gui.platform.setBlendMode("alpha", "premultiplied")

				eCont.element:render(eCont.props.x, eCont.props.y + self.vOffset)

				gui.platform.setBlendMode(a, b)
			end
		end

		if self.slider then
			self.slider:render(self.w - 25, 0 + 15)
		end
		self.redraw = false
		self.rerender = false
	end

	gui.platform.setStencilTest()
end

return frame