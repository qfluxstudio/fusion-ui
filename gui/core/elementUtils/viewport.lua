--[[ Viewport ]]
-- A subsystem of element
-- Handles internal element drawing, sizing
--[[--------------------------------------------------
	Fusion UI by qfx (qfluxstudios@gmail.com) 
	Copyright (c) 2017-2018 Elmārs Āboliņš
	gitlab.com/project link here 
----------------------------------------------------]]
local path = string.sub(..., 1, string.len(...) - string.len(".core.elementUtils.viewport"))
local gui = require(path .. ".dummy")
local calc = gui.ch.calcNum
local viewport = {}
viewport.__index = viewport

function viewport.new(elem, type, overflow, style, content)
    return setmetatable({
        root = elem,
        type = type,
        overflow = overflow,
        style = style,
        content = content,
        sizeUnset = true
    }, viewport)
end

function viewport:setSize(outerW, outerH)
    local pl = calc(self.style.padding[1], outerW, 0)
    local pt = calc(self.style.padding[2], outerH, 0)
    local pr = calc(self.style.padding[3], outerW, 0)
    local pb = calc(self.style.padding[4], outerH, 0)

    local ml = calc(self.style.margin[1], outerW, 0)
    local mt = calc(self.style.margin[2], outerH, 0)
    local mr = calc(self.style.margin[3], outerW, 0)
    local mb = calc(self.style.margin[4], outerH, 0)

    if self.type == 'block' then
        self.elemW = calc(self.style.width, outerW, 0)
        self.elemH = calc(self.style.height, outerH, 0)

        self.innerW = self.elemW - pl - pr
        self.innerH = self.elemH - pt - pb

        self.type:setSize(self.innerW, self.innerH)
        self.contentW, self.contentH = self.type:getSize()
    elseif self.type == 'inline' then
        self.maxW = outerW
        self.maxH = outerH

        self.type:setMaxSize(self.maxW, self.maxH)

        self.contentW, self.contentH = self.type:getSize()

        self.innerW = self.contentW
        self.innerH = self.contentH

        self.elemW = self.innerH + pl + pr
        self.elemH = self.innerH + pt + pb
    elseif self.type == 'absolute' then
        self.elemW = outerW
        self.elemH = outerH

        self.innerW = self.elemW - pl - pr
        self.innerH = self.elemH - pt - pb

        self.type:setSize(self.innerW, self.innerH)
        self.contentW, self.contentH = self.type:getSize()
    end

    self.footprintW = self.elemW + ml + mr
    self.footprintH = self.elemH + mt + mb

    self.sizeUnset = false
end

function viewport:getSize(w, h)
    if self.sizeUnset then
        self:setSize(w, h)
    end
    return self.footprintW, self.footprintH
end

function viewport:update(dt)
    self.type:update(dt)
end

function viewport:canvasInit()
    if not self.canvas then
        self.canvasSize = {
            w = math.ceil((self.elemW) * 1.5),
            h = math.ceil((self.elemH) * 1.5)
        }

        self.canvas = gui.platform.newCanvas(self.canvasSize.w, self.canvasSize.h)
    end

    if not self.quad then
        self.quad = love.graphics.newQuad(0, 0, self.elemW, self.elemH, self.canvasSize.w, self.canvasSize.h)
    else
        self.quad:setViewport(0, 0, self.elemW, self.elemH)
    end
end

function viewport:render()
    local r, g, b, a = self.canvasColor[1], self.canvasColor[2], self.canvasColor[3], self.canvasColor[4] / 255
    gui.platform.setColor(r * a, g * a, b * a, 255 * a)

    if self.canvas and self.quad then
        gui.platform.draw(self.canvas, self.quad, x, y)
    end
end

function viewport:draw()
    if not self.canvas or not self.quad then
        self:canvasInit()
    end

    gui.platform.setCanvas(self.canvas)

    gui.platform.clear()

    self.type:render(0, 0, self.cValues.w, self.cValues.h, self.content, self.style)

    gui.platform.setCanvas()
end

return viewport