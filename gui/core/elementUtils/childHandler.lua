--[[ Child handler ]]
-- Basically a submodule of element
--[[--------------------------------------------------
	Fusion UI by qfx (qfluxstudios@gmail.com)
	Copyright (c) 2017-2018 Elmārs Āboliņš
	gitlab.com/project link here
----------------------------------------------------]]
local path = string.sub(..., 1, string.len(...) - string.len(".core.childHandler"))
local gui = require(path ..'.elementUtils'.. ".dummy")
local childHandler = {}
childHandler.__index = childHandler

function childHandler.new(children, element, type)
    local ch = setmetatable({
        elements = {},
        root = element,
        type = type
    }, childHandler)

    if children then
        ch:addChildren(children)
    end

    return ch
end

function childHandler:addChildren(children, index)
    for i, eCont in ipairs(children) do
        eCont:addEventListener('changed', self.elementUpdate, self, i)
        eCont:addEventListener('stylechange', self.elementUpdate, self, i)
        eCont:addEventListener('styleswitch', self.elementUpdate, self, i)
        eCont:addEventListener('redrawn', self.elementRender, self, i)
        eCont:addEventListener('canvasinit', self.elementRender, self, i)
        if index then
            table.insert(self.elements, index, eCont)
            index = index + 1
        else
            self.elements[i] = eCont
        end
    end
end

function childHandler:calcSizes()

end

function childHandler:elementUpdate()
    self.mkLayout = true
end

function childHandler:elementRender()
    self.redraw = true
end

function childHandler:updateChildren()
    for i, eCont in ipairs(self.elements) do
        local p = eCont.props
        if not( p.y+self.vOffset-p.h>self.h or p.y-self.vOffset+p.h<0 ) or self.init then
            eCont:draw(p.x+x, p.y+y+self.vOffset, p.w, p.h, nil, false)
            eCont:update(0,false)
        elseif eCont.style.position=='fixed' then
            eCont:draw(p.x+x, p.y+y, p.w, p.h, nil, false)
            eCont:update(0,false)
        else
            eCont.type:cleanUp()
        end
        self.init = false
    end
end

return childHandler