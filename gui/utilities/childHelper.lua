local ch = {}
--The number, reference number, fallback number
function ch.calcNum(num, rn, fbn)
    if num:find('%%') then
        local x = num:gsub('%%','')
        return rn*(tonumber(x)/100)
    else
        local x = num:gsub('px','')
        local cal = tonumber(x)

        if cal and cal > 0 then
            return cal
        else
            return fbn
        end
    end
end

function ch.calcOrigin(origin, x, y, w, h)
    if origin=='start' then
        return x, y
    elseif origin=='center' then
        x = x - 0.5*w
        y = y - 0.5*h
    else
        x = x - w
        y = y - h
    end
end

function ch.get_sizes(prop, elem, fw, fh)
    local x, y, w, h
    
    fw = tonumber(fw)
    fh = tonumber(fh)

    elem.style.position = elem.style.position or 'normal'
    
    elemW, elemH = elem:getSize(fw, fh)

    if elem.style.size == 'block' then
        elemW = ch.calcNum(elem.style.width, fw, elemW)
        elemH = ch.calcNum(elem.style.height, fh, elemH)
    end

    if elem.style.position=='normal' then
        x = 0
        y = 'calculated'
    elseif elem.style.position=='absolute' then
        if elem.style.left~='' then
            x = ch.calcNum(elem.style.left, fw, 'calculated')
        elseif elem.style.right~='' then
            x = fw-elemW-ch.calcNum(elem.style.right, fw, 'calculated')
        end
        if elem.style.top~='' then
            y = ch.calcNum(elem.style.top, fh, 'calculated')
        elseif elem.style.bottom~='' then
            y = fh-elemH-ch.calcNum(elem.style.top, fh, 'calculated')
        end
    end

    return {x = x, y = y, w = elemW, h = elemH}
end

return ch