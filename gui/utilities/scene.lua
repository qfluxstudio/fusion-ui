local path = string.sub(..., 1, string.len(...) - string.len(".utilities.scene"))
local gui = require(path .. ".dummy")
local xml = require(path..".lib.xml").newParser()
local scene = {}
scene.__index = scene
 
function scene.new( sceneFile )
	local classes, types, elements, ids = {}, {}, {}, {}
	local scs
	local scen = {}
	local file = love.filesystem.read(sceneFile)

	if not file then
		print('file not found '..sceneFile)
		return nil
	end

	local dom = xml:ParseXmlText(file)

	local function printNode(table,whitespace)
		whitespace = whitespace..'	'
		for i, e in pairs(table) do
			if type(e) == 'table' then
				printNode(e, whitespace)
			else
				print(i..'['..type(e)..'] : '..tostring(e))
			end
		end
	end
	--printNode(dom,'')
	
	if not dom.scene then
		print('no scene node found '..sceneFile)
		return nil
	end

	if not dom.scene.frame then
		print('no frame node found '..sceneFile)
		return nil
	end

	if dom.scene.style and dom.scene.style.___props['@file'] then
		scs = gui.scs.new(dom.scene.style.___props['@file'])
	end

	for i, e in pairs(dom.scene.frame) do
	
	end

	if #dom.scene.frame > 1 then
		for i, frame in ipairs(dom.scene.frame) do
			scene.parseChild(frame, scs, classes, types, elements, ids)
		end
	else
		scen.frame = scene.parseChild(dom.scene.frame, scs, classes, types, elements, ids)
	end

	scen.classes = classes
	scen.types = types
	scen.elements = elements
	scen.ids = ids

	return setmetatable(scen, scene)
end

function scene.parseChild(node, scs, classes, types, elements, ids)
	local children, elem = {}, {}
	local id, class, style

	if node.___props['@id'] then
		id = node.___props['@id']
	end

	if node.___props['@class'] then
		class = node.___props['@class']
	end

	if scs then
		style = scs:compElem(node.___name, class, id)
	end

	if node.___children and node.___name=='frame' then
		for i, e in ipairs(node.___children) do
			table.insert(children, {element = scene.parseChild(e, scs, classes, types, elements, ids)})
		end
	
		elem = gui.element.newElement(node.___name, children, style)
	else
		elem = gui.element.newElement(node.___name, node.___value, style)
	end

	table.insert(elements, elem)

	if id then
		ids[id]=elem
		elem.id = id
	end

	if class then
		for cl in class:gmatch('[^ ]+ ') do
			if classes[cl] then
				table.insert(classes[cl], elem)
			else
				classes[cl]={elem}
			end

			if elem.classes then
				table.insert(elem.classes, cl)
			else
				elem.classes = {cl}
			end
		end
	end

	if types[node.___name] then
		table.insert(types[node.___name], elem)
	else
		types[node.___name] = {elem}
	end

	return elem
end

function scene:draw(x, y, w, h)
	self.frame:drawPersistent(x, y, w, h)
end

return scene