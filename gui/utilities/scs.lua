local scs = {}
scs.__index = scs
local origin = '/data/scs'
--Selectors:
--[[
	#id = {-- style here --}
	.class = {-- style here --}
	$variable = 10
	type = {-- style here --}
]]
--Since lua doesn't support these naming schemes, we'll have to do some pre-processing
--We'll be 'compiling' to lua, since it's tables are pretty close in syntax to css 
local tokens = {
	id = '#[^%-^%+^%/^%*^#^=^{^}^\n^ ^	]+',
	class = '%.[^%-^%+^%/^%*^%.^=^{^}^\n^ ^	]+',
	variable = '$[^%-^%+^%/^%*^$^=^{^}^\n^ ^	]+',
	type = '@[^%-^%+^%/^%*^=^{^}^\n^$^#^%.^ ^	]+',
	reservedTokens = {
		'if',
		'then',
		'else',
		'elseif',
		'end',
		'+',
		'-',
		'/',
		'*',
		'(',
		')',
		','
	}
}
local gEnv = {}
gEnv.import = function (filename)

end

function scs.new(filename)

	local str = love.filesystem.read(origin..'/'..filename)

	if not str then
		print('Empty file/not found: '..origin..'/'..filename)
		return nil
	end

	local env, compStr, func = scs.loadFile(str)

	if env then
		return setmetatable({ 
			--Output fields of the operation (kept for quick computation of end style)
			classes   = env.classes,
			ids       = env.ids, 
			variables = env.variables,
			types     = env.types,

			--The transpiled string, in case it needs to be reloaded(by loadstring) 
			transpiledStr = compStr,
		
			--The final function kept for shallow reloading
			func = func
		}, scs)
	end
end

function scs:compElem(type, class, id)
	local finStyle = {}
	
	if self.types[type] then
		finStyle = self.types[type]
	end

	if class then
		for i, styles in pairs(self.classes) do
			if class:find(i) then
				for i, e in pairs(styles) do
					finStyle[i] = e
				end
			end
		end
	end

	if self.ids[id] then
		for i, e in pairs(self.ids[id]) do
			finStyle[i] = e
		end
	end

	return finStyle
end

function scs.setOrigin(location)
	origin = location
end

function scs.loadFile(str)
	local env = {}
	env.ids = {}
	env.classes = {}
	env.variables = {}
	env.types = {}

	for ids in str:gmatch(tokens.id) do
		if not scs.checkTokens(ids) then
			local name = ids:gsub('#+','')
			str = str:gsub(ids, 'ids["'..name..'"]')
		end
	end

	for class in str:gmatch(tokens.class) do
		if not scs.checkTokens(class) then
			local name = class:gsub('%.+','')
			str = str:gsub(class, 'classes["'..name..'"]')
		end
	end

	for variable in str:gmatch(tokens.variable) do
		if not scs.checkTokens(variable) then
			local name = variable:gsub('$+','')
			str = str:gsub(variable, 'variables["'..name..'"]')
		end
	end

	for type in str:gmatch(tokens.type) do
		if not scs.checkTokens(type) then
			local name = type:gsub('@+','')
			str = str:gsub(type, 'types["'..name..'"]')
		end
	end

	_, f= pcall(loadstring,str)

	--print(str)

	if _ and f then
		setfenv(f, env)
		_, _ = pcall(f)
		return env, str, f
	else
		--print('Error ('..f..') loading a style sheet: ')--..filename..', dumping transpiled code')
		print(_)
		return nil
	end
	
end

function scs.checkTokens(str)
	local found = false

	for index, token in ipairs(tokens.reservedTokens) do
		if str:upper():find( token:upper(), 0, true) then
			found = true
		end
	end

	return found
end

return scs

--[[
	example smart cascading style

	$import('button')
	$import('mainMenu')

	#mainMenu = {
		padding = {10, 10, 10, 10}
	}
	#mainMenu

]]