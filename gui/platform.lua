-- Platform-agnostic functions
-- Functions in here can be modified by user
-- when needed (example: porting to newer version of LOVE)

local love = require("love")
local platform = {}

-- fusion-ui expects all these functions to have 0.10.0 behavior
platform.newImage = love.graphics.newImage
platform.getFont = love.graphics.getFont
platform.rectangle = love.graphics.rectangle
platform.stencil = love.graphics.stencil
platform.setStencilTest = love.graphics.setStencilTest
platform.setColor = love.graphics.setColor
platform.draw = love.graphics.draw
platform.line = love.graphics.line
local tempTable = {nil, stencil = true}
platform.setCanvas = love.graphics.setCanvas
platform.clear = love.graphics.clear
platform.newCanvas = love.graphics.newCanvas
platform.newText = love.graphics.newText
platform.setFont = love.graphics.setFont
platform.print = love.graphics.print
platform.setBlendMode = love.graphics.setBlendMode
platform.getBlendMode = love.graphics.getBlendMode

local loveVer = love.getVersion()
if loveVer == 11 then
	platform.setCanvas = function(fbo)
		if fbo ~= nil then
			tempTable[1] = fbo
			return love.graphics.setCanvas(tempTable)
		else
			return love.graphics.setCanvas()
		end
	end

	platform.setColor = function(r, g, b, a)
		if type(r) == "table" then
			return love.graphics.setColor(r[1] / 255, r[2] / 255, r[3] / 255, (r[4] or 255) / 255)
		else
			return love.graphics.setColor(r / 255, g / 255, b / 255, a / 255)
		end
	end
end



return platform
