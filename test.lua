lrdb = require "lrdb_server"

local gui = require 'gui'
love.window.setMode(1280,720,{vsync=false})

mgui = {}
mgui.secondaryMenu = {
	backgroundColor = {40,40,40,0},
	backgroundImage = love.graphics.newImage('data/images/play-menu.png'),
	padding = {'30px', '30px', '30px', '30px'}
}
mgui.secondaryButton={
	backgroundImage = love.graphics.newImage('data/images/secondary_button.png'),
	margins = {'10px','10px','10px','10px'},
	backgroundSize = 'center',
	backgroundColor = {220,220,220,0},
	--backgroundImageColor = {250, 250, 250, 255},
	foregroundColor= {25, 25, 25, 255},
	font = love.graphics.newFont('/data/fonts/saxmono.ttf', 25),
	text = {
		align = 'center',
	}
}
local secondaryMenuClass = gui.template.new('frame', mgui.secondaryMenu)
local secondaryButtonClass = gui.template.new('button', mgui.secondaryButton)
secondaryMenu2 = secondaryMenuClass:newElement({
		{element=secondaryButtonClass:newElement('Load save1'), index='button1'},
		{element=secondaryButtonClass:newElement('Load save2'), index='button2'},
		{element=secondaryButtonClass:newElement('Load save3'), index='button3'},
		{element=gui.element.newElement('textBox','',{})},
		{element=gui.element.newElement('slider',{},{})},
		{element=gui.element.newElement('checkbox',{},{})}
})

function love.load()
	--[[x = gui.scs.new('menu.style')
	frm = x:compElem('frame')
	menu = x:compElem('','menu element', '')
	thing = x:compElem('','','thing')
	dom = gui.scene.new('/data/scenes/sample.xml')]]
	secondaryMenu2:drawPersistent(140,200, 1000,400)
end

function love.update( dt )
	
end

function love.draw()
	love.graphics.reset()
	local n=0
	if gui.avgtimers then
		for i, e in pairs(gui.avgtimers) do
			love.graphics.print(i..':'..string.format("%0.3f", (e*1000)),0, 15*n)
			n = n +1
		end
	end
end

local origErrHandler = love.errhand

function love.errhand(msg)
	lrdb.activate(21110)
	origErrHandler(msg)
end

function love.keypressed(key)
	
	if key=='f12' then
		love.graphics.print('Waiting for debug connection')
		lrdb.activate(21110)
	end

	if key=='f5' then
		assertionCounter = 1
	end
end