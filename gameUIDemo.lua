
lrdb = require "lrdb_server"
lrdb.activate(21110)
local gui = require 'gui'
love.window.setMode(1280,720,{vsync=false})
local wW, wH = love.window.getMode()
local menuSwitcher = nil
local menuSwitched

local mgui = {}
mgui.mainButton = {
	backgroundImage = love.graphics.newImage('data/images/btn_bg.png'),
    margins = {10,10,10,10},
    backgroundSize = 'cover',
    backgroundColor = {220,220,220,0},
	backgroundImageColor = {250, 250, 250, 255},
    foregroundColor= {25, 25, 25, 255},
    font = love.graphics.newFont('/data/fonts/saxmono.ttf', math.ceil(wH*0.035)),
    text = {
        align = 'center',
    }
}
mgui.play = {
	backgroundImage = love.graphics.newImage('data/images/btn_bg.png'),
    margins = {10,10,10,10},
    backgroundSize = 'cover',
    backgroundColor = {120,220,120,0},
	backgroundImageColor = {120, 250, 120, 255},
    foregroundColor= {25, 25, 25, 255},
    font = love.graphics.newFont('/data/fonts/saxmono.ttf', math.ceil(wH*0.035)),
    text = {
        align = 'center',
    }
}
mgui.mainButtonOver = {
	backgroundImage = love.graphics.newImage('data/images/btn_bg.png'),
    margins = {10,10,10,10},
    backgroundSize = 'cover',
    backgroundColor = {0,0,0,0},
    backgroundImageColor = {100, 100, 100, 255},
    foregroundColor= {250, 250, 250, 255},
    font = love.graphics.newFont('/data/fonts/saxmono.ttf', math.ceil(wH*0.035)),
    text = {
        align = 'center',
    }
}
mgui.mainButtonPressed = {
	backgroundImage = love.graphics.newImage('data/images/btn_bg.png'),
    margins = {10,10,10,10},
    backgroundSize = 'cover',
    backgroundColor = {0,0,0,0},
    backgroundImageColor = {50, 50, 50, 255},
    foregroundColor= {25, 25, 25, 255},
    font = love.graphics.newFont('/data/fonts/saxmono.ttf',math.ceil(wH*0.035)),
    text = {
        align = 'center',
    }
}
mgui.frame = {
	backgroundSize = 'cover',
	backgroundColor = {0,0,0,0},
	backgroundImage = love.graphics.newImage('data/images/scr_bg.png'),
}
mgui.banner = {
	backgroundSize = 'cover',
	backgroundColor = {0,0,0,0},
	backgroundImage = love.graphics.newImage('data/images/banner.png'),
	margins = {0,0,0,0},
}
mgui.secondaryMenu = {
	backgroundColor = {0,0,0,0},
	backgroundSize = 'cover',
	backgroundImage = love.graphics.newImage('data/images/play-menu.png'),
	padding = {70, 70, 70, 70}
}
mgui.secondaryButton={
	backgroundImage = love.graphics.newImage('data/images/secondary_button.png'),
    margins = {10,10,10,10},
    backgroundSize = 'cover',
    backgroundColor = {220,220,220,0},
	backgroundImageColor = {250, 250, 250, 255},
    foregroundColor= {25, 25, 25, 255},
    font = love.graphics.newFont('/data/fonts/saxmono.ttf', math.ceil(wH*0.035)),
    text = {
        align = 'center',
    }
}
mgui.title = {
	backgroundColor = {0,0,0,0},
	foregroundColor = {0, 0, 0, 255},
	font = love.graphics.newFont('/data/fonts/Nunito.ttf', math.ceil(wH*0.065)),
	text = {
        align = 'center',
    }
}
mgui.animOnDrawButton = function (style, prog, event, element)
	--element.hoffset = (1-prog)*-element.h
	element.yoffset = -150+150*prog
	--element.yoffset = -300+300*prog
	element.canvasColor[4] = 255*prog
end
mgui.animUnDrawButton = function (style, prog, event, element)
	--element.hoffset = (1-prog)*-element.h
	element.yoffset = 300*prog
	--element.yoffset = -300+300*prog
	element.canvasColor[4] = 255-255*prog
end

local frameClass = gui.template.new('frame', mgui.frame)
local bannerClass = gui.template.new('button', mgui.banner)
local buttonClass = gui.template.new('button', mgui.mainButton)
local secondaryMenuClass = gui.template.new('frame', mgui.secondaryMenu)
secondaryMenuClass:addAnimation('canvasinit',mgui.animOnDrawButton,0.4)
secondaryMenuClass:addAnimation('undrawCue',mgui.animUnDrawButton,0.4)
secondaryMenuClass:addEventListener('animationFinish', function (event, element)
	if event == 'undrawCue' then
		if element==menuSwitcher then
			menuSwitcher=nil
		end
		element:unDraw()
	end
end)
secondaryMenuClass:addEventListener('dragged',function (event, element) 
	element.x = element.x + event.dx
	element.y = element.y + event.dy 
end)
local secondaryButtonClass = gui.template.new('button', mgui.secondaryButton)
secondaryButtonClass:addAnimation('canvasinit',mgui.animOnDrawButton,0.4)
secondaryButtonClass:addEventListener('entered', function (event, element)
	print('entered')
end)
local secondaryMenu = secondaryMenuClass:newElement({
	elements = {
		{element=secondaryButtonClass:newElement('Play Mode1'), index='button1'},
		{element=secondaryButtonClass:newElement('Play Mode2'), index='button2'},
		{element=secondaryButtonClass:newElement('Play Mode3'), index='button3'},
	},
	layout = {
		button1={
			position = 'relative',
			left = 6,
			bottom =35,
			size = 'relative',
			w = 25;
			h = 30;
		},
		button2={
			position = 'relative',
			left = 40,
			bottom = 35,
			size = 'relative',
			w = 25;
			h = 30;
		},
		button3={
			position = 'relative',
			left = 70,
			bottom = 35,
			size = 'relative',
			w = 25;
			h = 30;
		}
	}
})

local secondaryMenu2 = secondaryMenuClass:newElement({
	elements = {
		{element=secondaryButtonClass:newElement('Load save1'), index='button1'},
		{element=secondaryButtonClass:newElement('Load save2'), index='button2'},
		{element=secondaryButtonClass:newElement('Load save3'), index='button3'},
		{element=gui.element.newElement('textBox','',{})},
		{element=gui.element.newElement('slider',{},{})},
		{element=gui.element.newElement('checkbox',{},{})}
	},
	layout = {
		button1={
			position = 'relative',
			left = 6,
			bottom =35,
			size = 'relative',
			w = 25;
			h = 30;
		},
		button2={
			position = 'relative',
			left = 40,
			bottom = 35,
			size = 'relative',
			w = 25;
			h = 30;
		},
		button3={
			position = 'relative',
			left = 70,
			bottom = 35,
			size = 'relative',
			w = 25;
			h = 30;
		}
	}
})
buttonClass:addAnimation('canvasinit',mgui.animOnDrawButton,0.4)
bannerClass:addAnimation('canvasinit',mgui.animOnDrawButton,0.4)
local playButtonClass = gui.template.new('button', mgui.play)
playButtonClass:addAnimation('canvasinit',mgui.animOnDrawButton,0.4)
local titleClass = gui.template.new('text', mgui.title)
titleClass:addAnimation('canvasinit',mgui.animOnDrawButton,0.4)


local title = titleClass:newElement('Title text')
local banner = bannerClass:newElement('')
local button1 = playButtonClass:newElement('Play')
button1:addEventListener('pressed', function (event, element)
	if menuSwitcher then
		menuSwitcher:emitEvent('undrawCue')
	end
	menuSwitcher = secondaryMenu
	menuSwitched = true
end)
local button2 = buttonClass:newElement('Load')
button2:addEventListener('pressed', function (event, element)
	if menuSwitcher then
		menuSwitcher:emitEvent('undrawCue')
	end
	menuSwitcher = secondaryMenu2
	menuSwitched = true
end)
local button3 = buttonClass:newElement('Settings')
local button4 = buttonClass:newElement('Exit')
button4:addEventListener('pressed', function (event, element)
	print(#gui.mouseBoxes)
end)

local frame = frameClass:newElement(
	{
		elements = {
			{element = banner, index = 'banner'},
			{element = title, index = 'title'},
			{element = button1, index = 'play'},
			{element = button2, index = 'load'},
			{element = button3, index = 'settings'},
			{element = button4, index = 'exit'},
		},
		layout = {
			play = {
				position = 'relative',
				top = 0,
				left = 41,
				size = 'relative',
				w = 20,
				h = 20,
			},
			load = {
				position = 'relative',
				top = 0,
				left = 26,
				size = 'relative',
				w = 13,
				h = 17,
			},
			settings = {
				position = 'relative',
				top = 0,
				left = 13,
				size = 'relative',
				w = 13,
				h = 17,
			},
			exit = {
				position = 'relative',
				top = 0,
				left = 0,
				size = 'relative',
				w = 13,
				h = 17,
			},
			title = {
				position = 'relative',
				top = 5,
				right = 30,
			},
			banner = {
				position = 'relative',
				top = 0,
				left = 0,
				size = 'absolute',
				w = wW,
				h = wW*0.15625
			}
		}
	}
)
local root, el
function love.load()
	root, el = {}, {}

end

function love.draw()

end

frame:drawPersistent(0, 0, wW, wH)

function love.update(dt)
	if menuSwitched then
		menuSwitcher:drawPersistent((wW/2)-(wW*(0.78125/2)),(wH/2)-(wH*(0.55/2)), wW*0.78125,wH*0.55)
		menuSwitched = false
	end

	root = {}
    
    for i, e in pairs(button1) do
        table.insert(root, i)
    end
    if button1.prevEvents then
        for i, e in pairs(el) do
            e.time = e.time+dt
        end
        for i, e in pairs(button1.prevEvents) do
            if el[i] then
                el[i].time = 0
            else
                el[i] = {}
                el[i].time = 0
            end
        end
    end
end

function love.keypressed(key)
	if key =='e' then
		
	else
		local stats = love.graphics.getStats()
		local str = string.format("Vram usage: %.2f MB", stats.texturememory / 1024 / 1024)
		print(str)
		print(love.timer.getFPS())
		local n=0
		if gui.avgtimers then
			for i, e in pairs(gui.avgtimers) do
				print(i..':'..e*1000)
				n = n +1
			end
		end
	end
end