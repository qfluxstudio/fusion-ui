Fusion UI is a comprehensive and flexible suite for the www.love2d.org framework.

The goals of this project:
- Very low overhead and great performance on any system
- Flexible styling and element configurations
- Advanced animations

The home of the project is https://gitlab.com/qfluxstudio/fusion-ui

To start using this library download the repo, copy the gui folder in to your project and "require 'gui'" it.

Currently supports 0.10.2 out of the box, can be adapted to 11.*